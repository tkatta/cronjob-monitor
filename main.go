package main

import (
	"fmt"
	"log"
	"os/exec"
	"runtime"
	"strings"
)

var CronJobs []string
var CronUsers = []string{"gcid", "gitlab"}

func windowsCronJobs() {
	// Run the schtasks command to list all tasks
	cmd := exec.Command("schtasks", "/query", "/fo", "csv", "/v")

	// Execute the command and capture the output
	output, err := cmd.Output()
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	// Split the output into lines
	lines := strings.Split(string(output), "\n")

	// Print each line (each task)
	for _, line := range lines {
		fmt.Println(line)
	}
}

func getCronJobs() []string {

	for _, cronUser := range CronUsers {

		//run the crontab command to find the cron jobs
		cmd := exec.Command("crontab", "-u", cronUser, "-l")
		//capture the output of crontab command
		output, err := cmd.Output()
		if err != nil {
			log.Fatal("Error:", err)
		}
		// convert the recieved output and store the each cron job in a list of string
		lines := strings.Split(string(output), "\n")

		for _, line := range lines {
			if line != "" && !strings.HasPrefix(line, "#") {
				CronJobs = append(CronJobs, line)
			}
		}

	}
	return CronJobs

}

func main() {

	osType := runtime.GOOS
	fmt.Println("Operating System:", osType)
	if strings.Contains(strings.ToLower(osType), "linux") {
		log.Println("Scheduled Crob Jobs are:", getCronJobs())
	} else {
		windowsCronJobs()
	}
}
